<!-- **Basic syntax -->
<!-- *PHP tags -->

<?php echo 'Hello World'?>

<?= 'You can use the short echo tag to' ?>


<!--HTML in PHP   -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=s, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <p>This is a paragraph</p>
  <?php echo "<p>This is a paragraph with PHP</p>" //This a comment!;?>
  <h1><?php
  //This a comment!
   echo 'Header with PHP!';
  /* 
    This is also comment
    which work multiple lines!
  */
   ?></h1>
  <p>This is a paragraph</p>

  
</body>

</html>


<?php
echo 'Line one' 
?>

<?php echo "Some text"; ?>
No newline
<?= "But newline now" ?> 
            

<!-- --------------------------------------------------------------------- -->
<!-- **Variables -->

<?php
// We use $ to define a variable 
$name = 'Soheyl';
$lastName = 'Asbaghi';
echo "<p>My full name is $name $lastName </p>";

$num_1 = 2;
$num_2 = 4;
echo "<p>$num_1, $num_2 are Even </p>";

/* Predefined Variables
!Superglobals — Superglobals are built-in variables that are always available in all scopes
$GLOBALS — References all variables available in global scope
$_SERVER — Server and execution environment information
$_GET — HTTP GET variables
$_POST — HTTP POST variables
$_FILES — HTTP File Upload variables
$_REQUEST — HTTP Request variables
$_SESSION — Session variables
$_ENV — Environment variables
$_COOKIE — HTTP Cookies
$php_errormsg — The previous error message
$http_response_header — HTTP response headers
$argc — The number of arguments passed to script
$argv — Array of arguments passed to script
*/

/*These super global variables are:

$GLOBALS
$_SERVER
$_GET
$_POST
$_FILES
$_COOKIE
$_SESSION
$_REQUEST
$_ENV

*/

// global and local scope -> global var to local var
function Sum()
{
    global $num_1, $num_2;

    $num_2 = $num_1 + $num_2;
} 

Sum();
var_dump($num_2);


// $GLOBALS Array 
$a = 1;
$b = 2;

function Sum_1()
{
    $GLOBALS['b'] = $GLOBALS['a'] + $GLOBALS['b'];
} 

Sum_1();
echo "<br/>";
var_dump($b);


// Static -> local var to global var
function test()
{
    static $c = 0;
    echo $c;
    $c++;
}

echo "<br/>";
test();
echo "<br/>";
test();
echo "<br/>";
test();
echo "<br/>";

// Variable Variables

$d = 'Hello';
$$d = 'World';
echo "$d ${$d}";
echo "<br/>";


$one = 'two';
$two = 'three';
$three = 'four';
$four = 'five';
echo $$$$one;
?>

<!-- --------------------------------------------------------------------- -->
<!-- **Types -->

<?php

/*
PHP supports ten primitive types.

Four scalar types:
bool
int
float (floating-point number, aka double)
string

Four compound types:
array
object
callable
iterable

And finally two special types:
resource
NULL
*/

// Boolean ->

/*
!When converting to bool, the following values are considered false:
the boolean false itself
the integers 0 and -0 (zero)
the floats 0.0 and -0.0 (zero)
the empty string, and the string "0"
an array with zero elements
the special type NULL (including unset variables)
SimpleXML objects created from attribute less empty elements, i.e. elements which have neither children nor attributes.
*/

// true = 1
// false = 0

var_dump((bool) "");        // bool(false)
var_dump((bool) 1);         // bool(true)
var_dump((bool) -2);        // bool(true)
var_dump((bool) "foo");     // bool(true)
var_dump((bool) 2.3e5);     // bool(true)
var_dump((bool) array(12)); // bool(true)
var_dump((bool) array());   // bool(false)
var_dump((bool) "false");   // bool(true)

// --------------------------------------
// Integers ->
// !An int is a number of the set ℤ = {..., -2, -1, 0, 1, 2, ...}.

$int = 1234; // decimal number
$int = 0123; // octal number (equivalent to 83 decimal)
$int = 0x1A; // hexadecimal number (equivalent to 26 decimal)
$int = 0b11111111; // binary number (equivalent to 255 decimal)
$int = 1_234_567; // decimal number (as of PHP 7.4.0)

// --------------------------------------
// Floating point numbers ->

$flo = 1.234; 
$flo = 1.2e3; 
$flo = 7E-10;
$flo = 1_234.567; // as of PHP 7.4.0
echo "<br/>";
// --------------------------------------
// String ->

$firstName = 'Soheyl';
echo $firstName[0]; // First character in $firstName
echo "<br/>";
$lastName = 'Asbaghi';
$stgTest = "this is for \"Test\" ";
echo substr($stgTest, 4, 7);
// echo $stgTest = 'this is for "Test"';

// $fullName = $firstName . $lastName;
// $fullName = $firstName . ' ' . $lastName;
// $fullName = $firstName . '@' . $lastName;

echo $fullName;
// Numeric strings 
// $numStg = 1 + "10.5";       
// $numStg = 1 + "-1.3e3";              
// $numStg = 1 + "bob-1.3e3"; 

/* Escaped characters
\n	linefeed (LF or 0x0A (10) in ASCII)
\r	carriage return (CR or 0x0D (13) in ASCII)
\t	horizontal tab (HT or 0x09 (9) in ASCII)
\v	vertical tab (VT or 0x0B (11) in ASCII)
\e	escape (ESC or 0x1B (27) in ASCII)
\f	form feed (FF or 0x0C (12) in ASCII)
\\	backslash
\$	dollar sign
\"	double-quote
*/
echo "<br/>";

// --------------------------------------
// Array -> array()

$array_1 = array('a', 'b', 'c', 1);
var_dump($array_1[0]);
echo "<br/>";

// $array = array(
//   1 => "id",
//   "name" => "soheyl",
//   "family" => "Asbaghi",
//   "age" => 27
// );

// Using the short array syntax
$array = [
  1 => "id",
  "name" => "soheyl",
  "family" => "Asbaghi",
  "age" => 27
];

var_dump($array);
var_dump($array['age']);
var_dump($array[1]);

// Null -> null 